var Wallet = require("../models/wallet");
var uuidV4 = require("../utils/utils");

module.exports = {

    add_wallet: function (data, callback) {
        var wallet_id = uuidV4.getUniqueId();
        walletData = function () {
            Wallet.findOne({
                "wallet_id": wallet_id
            }, function (err, walletdata) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding wallet_id database error"
                    });
                } else if (walletdata != null) {
                    callback({
                        statuscode: 304,
                        msg: "Wallet already exist"
                    });

                } else {
                    var addwallet = new Wallet();
                    addwallet.wallet_id = wallet_id;
                    addwallet.description = data.description;
                    addwallet.income_expense = data.income_expense;
                    addwallet.amount = data.amount;
                    addwallet.summary = data.summary;
                    addwallet.transation_date = data.transation_date;
                    addwallet.save(function (err, result) {
                        if (err) {
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "saving Wallet, database error"
                            });
                        } else {
                            callback({
                                statuscode: 200,
                                msg: "Wallet added successfully",
                                data: addwallet
                            });
                        }
                    });
                }
            });
        }
        process.nextTick(walletData);
    },

    get_wallet: function (data, callback) {
        walletData = function () {
            Wallet.findOne({
                "wallet_id": data.wallet_id
            }, function (err, wallet) {
                if (err) {
                    callback({
                        msg: "Finding wallet_id from database , an error",
                        statuscode: 500,
                    });
                } else if (wallet == null) {
                    callback({
                        msg: "wallet_id does not exist",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "Wallet details",
                    statuscode: 200,
                    data: wallet
                });
            });
        }
        process.nextTick(walletData);
    },

    list_wallets: function (callback) {
        walletData = function () {
            Wallet.find({}, function (err, wallets) {
                if (err) {
                    callback({
                        msg: "Finding wallets from database , an error",
                        statuscode: 500,
                    });
                } else if (wallets == null) {
                    callback({
                        msg: "No wallets found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of wallets",
                    statuscode: 200,
                    data: wallets.slice().sort((a, b) => new Date(b.created_at) - new Date(a.created_at))
                });
            });
        }
        process.nextTick(walletData);
    },

    update_wallet: function (data, callback) {
        walletData = function () {
            if (data.wallet_id == undefined || data.wallet_id == '') {
                callback({
                    msgkey: "Wallet not found",
                    statuscode: 404,
                });
            } else {
                Wallet.findOne({
                    "wallet_id": data.wallet_id,
                }, function (err, walletdata) {
                    var newdata = walletdata;
                    if (err) {
                        callback({
                            statuscode: 500,
                            msg: "Finding wallet_id database error "
                        });
                    } else if (walletdata == null) {
                        callback({
                            statuscode: 404,
                            msg: "wallet_id not found "
                        });
                    } else {
                        if (data.description !== undefined && data.description !== "") {
                            newdata.description = data.description;
                        }
                        if (data.income_expense !== undefined && data.income_expense !== "") {
                            newdata.income_expense = data.income_expense;
                        }
                        if (data.amount !== undefined && data.amount !== "") {
                            newdata.amount = data.amount;
                        }
                        if (data.summary !== undefined && data.summary !== "") {
                            newdata.summary = data.summary;
                        }
                        if (data.transation_date !== undefined && data.transation_date !== "") {
                            newdata.transation_date = data.transation_date;
                        }
                        newdata.updated_at = Date.now();
                        newdata.save(function (err, walletdata) {
                            callback({
                                statuscode: 404,
                                msg: "Wallet updated Successfully ",
                                data: walletdata
                            });
                        });
                    }
                });
            }
        }
        process.nextTick(walletData);
    },

    delete_wallet: function (data, callback) {
        walletData = function () {
            if (data.wallet_id == undefined || data.wallet_id == '') {
                callback({
                    msgkey: "Wallet not found",
                    statuscode: 404,
                });
            } else {
                Wallet.deleteOne({
                    "wallet_id": data.wallet_id
                }, function (err, wallet) {
                    if (err) {
                        callback({
                            msg: "Finding wallet_id from database , an error",
                            statuscode: 500,
                        });
                    } else if (wallet == null) {
                        callback({
                            msg: "wallet_id does not exist",
                            statuscode: 404,
                        });
                    }
                    callback({
                        msg: "Wallet deleted successfully",
                        statuscode: 200,
                    });
                });
            }
        }
        process.nextTick(walletData);
    }

}