var mongoose = require('mongoose');
module.exports = mongoose.model('Registration', {
    id: String,
    password: String,
    firstname:String,
    lastname:String,
    pdata: String,
    createdAt: String,
    expiresAt: String,
    role:String,
    otpExpiry: String,
    token: String,
    isActive: String,
    username: {type: String, index: true}
});
