var mongoose = require('mongoose');
module.exports = mongoose.model('wallet', {
    wallet_id: String,
    description: String,
    income_expense: String,
    amount: String,
    summary: String,
    created_at: {type: Date, default: Date.now},
    updated_at: Date,
    transation_date: Date,
    is_active : {type: Boolean, default: true}
});