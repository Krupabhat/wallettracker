var bCrypt = require('bcrypt-nodejs');
var async = require('async');
var express = require('express');
var multiparty = require('multiparty');
var fs = require('fs');
var path = require('path');
var expressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');
var email = require("../../utils/mailer");
var router = express.Router();
var BASE_API_URL = "";
var version = "1.0"; // version code
/* congig */
var configuration = require("../../config");
/* utils*/

/* handler */
var WalletHandler = require("../../handlers/wallet_handler");
/* Generates hash using bCrypt*/
var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

/* Authentication function*/
var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) {
        next();
    }
    // if the user is not authenticated then redirect him to the login page
    res.redirect(BASE_API_URL + '/');
};

var isAuthenticatedAccessToken = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers.authorization;
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, configuration.TOKEN_SECRET, function (err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                //console.log(decoded)
                req.user = decoded;
                next();
            }
        });
    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            statuscode: 203,
            msgkey: "api.access.token.failed",
            v: version
        });
    }
}

module.exports = function (passport) {

    /*API to accress root*/
    router.get(BASE_API_URL + '/', function (req, res) {

        var reponseJson = {
            statuscode: 200,
            msgkey: "login.first.to.access.api",
            v: version
        };
        res.json(reponseJson);
    });

    /* sign up user exist*/
    router.get(BASE_API_URL + '/_signupfailure', function (req, res) {

        var reponseJson = {
            statuscode: 203,
            msgkey: "auth.signup.exist",
            v: version
        };
        res.json(reponseJson);
    });

    /* API for Login*/
    router.post(BASE_API_URL + '/login', function (req, res, next) {
        passport.authenticate('login', function (err, user, info) {
            if (err) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "login failure",
                    v: version
                };
                res.json(reponseJson);
            } else if (info) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "login failure",
                    v: version
                };
                res.json(reponseJson);

            } else {
                var reponseJson = {
                    statuscode: 200,
                    msgkey: "login success",
                    v: version,
                    data: user
                };
                res.json(reponseJson);
            }
        })(req, res, next);
    });

    /* API for GET Home Page */
    router.get(BASE_API_URL + '/home', function (req, res) {
        if (req.user.username) {
            var reponseJson = {
                statuscode: 200,
                msgkey: "login.success",
                v: version,
                data: req.user
            };
        }
        res.json(reponseJson);
    });

    /* API for login failure*/
    router.get(BASE_API_URL + '/_loginfailure', function (req, res) {
        var reponseJson = {
            statuscode: 203,
            msgkey: "login.failure",
            registered: false,
            v: version
        };
        res.json(reponseJson);
    });

    /* API for Handle Logout */
    router.get(BASE_API_URL + '/logout', function (req, res) {
        req.session.destroy(function (err) {
            var reponseJson = {
                statuscode: 200,
                msgkey: "logout.success",
                v: version
            };
            res.json(reponseJson);
        });

    });

    /* API for Handle Registration POST */
    router.post('/signup', function (req, res, next) {
        passport.authenticate('signup', function (err, user, info) {
            if (err) {
                //  console.log("Error 1: ", err);
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "auth.signup.exist",
                    v: version
                };
                res.json(reponseJson);
            } else if (info) {
            } else {
                var registered = false;
                var reponseJson = {
                    statuscode: 200,
                    msgkey: "auth.signup.success",
                    username: user.username,
                    v: version
                };

                // res.render('home', { user: req.user });
                req.logout();
                res.json(reponseJson);
            }
        })(req, res, next);
    });

    /* API for signup success page */
    router.get(BASE_API_URL + '/_signupsuccess', function (req, res) {
        var registered = false;
        if (req.user.isActive === "1") {
            registered = true;
        }

        var reponseJson = {
            statuscode: 200,
            msgkey: "auth.signup.success",
            registered: registered,
            username: req.user.username,
            v: version
        };
        // res.render('home', { user: req.user });
        req.logout();
        res.json(reponseJson);
    });

    router.post(BASE_API_URL + '/add_wallet', function (req, res) {
        var data = req.body;
        WalletHandler.add_wallet(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_wallet', function (req, res) {
        var data = req.query;
        WalletHandler.get_wallet(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_wallets', function (req, res) {
        WalletHandler.list_wallets(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.put(BASE_API_URL + '/update_wallet', function (req, res) {
        var data = req.body;
        WalletHandler.update_wallet(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.delete(BASE_API_URL + '/delete_wallet', function (req, res) {
        var data = req.body;
        WalletHandler.delete_wallet(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    return router;
};