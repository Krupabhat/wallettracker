module.exports = {
    "APPLICATION_PORT": 9000,
    "TOKEN_SECRET": "!@wallet_tracker@!",
    "TOKEN_EXPIRY": 525600,
    "ALERT_EMAIL": "mail@avohi.com",
    "VERSION_V1_URL": '/wallet_tracker/api/v1/',
    "SMS_CAMPAIGN_NAME": "wallet_tracker",
    "SMS_SENDER_NAME": "wallet_tracker",
    "SENDER_EMAIL": "",
    "EMAIL_PASSWORD": "",
    "GMAIL_PASSWORD":""
}
